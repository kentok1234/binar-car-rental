# binar-car-rental

## Binar Car Rental Website
Choose a self-explaining name for your project.

## Description
Car rental services that provide a variety of quality cars at affordable prices

## Installation
clone this project to your local device

USING HTTPS:
```
git clone https://gitlab.com/kentok1234/binar-car-rental.git
```

USING SSH:
```
git clone git@gitlab.com:kentok1234/binar-car-rental.git
```

Go to the directory that was cloned earlier
```
cd <directory name>
```

If you use PHP for your server you can use this command
```
php -S localhost:8080
```
The port according to what you want, besides that you can also just by clicking 2 times on the index.html file.

## Project status
DEVELOPMENT STAGE
