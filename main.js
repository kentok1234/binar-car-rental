const carContainer = document.getElementById("car-container")
const headerEl = document.querySelector('header')
const navbarEl = document.getElementById('navbarToggle')
const buttonToggler = document.querySelector('.navbar-toggler')

const imageWomanFront = document.getElementById('girl-front')
const containerImageWoman = imageWomanFront.parentElement
const parentImageWoman = imageWomanFront.parentElement.parentElement
let widthParentWoman = parentImageWoman.offsetWidth
let heightParentWoman = parentImageWoman.offsetHeight
const widthWindow = window.innerWidth
const parallaxCar = 0.2


// Position Image Girl
imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.5)) + 'px'
imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'

if ((widthWindow > 992) && (widthWindow < 1200)) {
    imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.45)) + 'px'
}

window.addEventListener('resize', () => {
    const scrollY = window.scrollY
    
    // when resize, change position image girl
    widthParentWoman = parentImageWoman.offsetWidth
    heightParentWoman = parentImageWoman.offsetHeight

    carContainer.style.transform = 'translate(0%,' + scrollY * parallaxCar + 'px)'
    if (widthWindow > 992) {
        carContainer.style.transform = 'translateY(' + scrollY * parallaxCar + 'px)'

        if (widthWindow < 1200) {
            imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.45)) + 'px'
            imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'
        } else {
            imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.5)) + 'px'
            imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'
        }
    } else {
        imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.5)) + 'px'
        imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'
    }

    
})

window.addEventListener('scroll', (e) => {
    const widthWindow = window.innerWidth
    const scrollY = window.scrollY

    if (scrollY > 0) {
        headerEl.classList.add('scroll-header')
        carContainer.style.transform = 'translate(0%,' + scrollY * parallaxCar + 'px)'
        if (widthWindow > 992) {
            carContainer.style.transform = 'translateY(' + scrollY * parallaxCar + 'px)'
        }
    }
    else {
        if (!navbarEl.classList.contains('show')) {
            headerEl.classList.remove('scroll-header')
        }
    }
})

buttonToggler.addEventListener('click', () => {
    const scrollY = window.scrollY

    if (scrollY == 0) {
        headerEl.classList.toggle('scroll-header')
    }
})

// Carousel
// const myCarousel = document.querySelector("#carouselExampleControls")
// const carousel = new bootstrap.Carousel(myCarousel, {
//     interval: 2000,
//     wrap: false
// })

